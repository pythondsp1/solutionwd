.. Solution (WD) documentation master file, created by
   sphinx-quickstart on Tue Mar  7 05:23:13 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Solution (WD)
=============

.. admonition:: Codes and Datasets
   :class: danger
   
   The datasets and the codes of the tutorial can be downloaded from the `repository <https://bitbucket.org/pythondsp/solutionwd/downloads/>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   solution/index


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`


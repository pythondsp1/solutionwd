# fast_solution.py

import numpy as np

def ETA(data):
    """ calculate next meltdown for data

        outputs:
            $ echo -e "4 3 7\n15 2 4 3 6\n16 2 4 3 6" | python sys_input.py
            > 9
            > 12
            > 14

            $ python file_input.py 
            9
            12
            14
            590619548
            181210921829
            204221389795918291262975
            323
    """

    # extract 'memory' and' 'process-duration (pd)'
    memory = data[0]   # memory
    pd = data[1:]      # process-duration
    pd = np.array(pd)  # convert to numpy arrays


    # initialization    
    current = memory           # current ETA
    prev_c  = memory//2        # previous ETA 
    next_c = current           # next ETA
    sum_memory = sum(current//pd)  # sum of memory consumed

    ## test: uncomment below to quick checks for answers
    ## and run the command $ python file_input.py
    ## put 'current' and 'current+1' below, to see the correctness of the code
    # sum_memory = sum(324//pd)  
    # return sum_memory


    # if sum_memory is less than memory,
    # then increase the initial count as bisection-search
    # will go above the memory. e.g. case 1 i.e. 4 3 7
    i=1
    while sum_memory < memory:
        current, prev_c = i * next_c, current
        sum_memory = sum(current//pd)
        i = i + 1


    while True:
        sum_memory = sum(current//pd)
        # print(memory, current, prev_c, next_c, sum_memory)

        # store 'current' as 'next_c' and recalculate the current.
        # next_c is used because after calculation 'current' value will be
        # less than itself.
        if sum_memory > memory:
            next_c, current = current, (prev_c + current)//2

            # set prev_c to 0, if all equal to continue the search
            if current == prev_c and prev_c==next_c:
                prev_c = 0

        elif sum_memory < memory:
            prev_c, current = current, (next_c+current)//2

            # return current if current and prev_c are equal
            # example line 4 of the file 'proc_disk_usage.data'
            # memory, current, prev_c, sum_memory
            # 1000000000000 181210921830 181210921829 1000000000016
            # 1000000000000 181210921829 181210921829 999999999979
            if current == prev_c:
                return current

        # decrease value of current and check again till condition
        # sum_memory == memory is valid. Ex. 15 2 4 5 6
        elif sum_memory == memory:
            # print(current)
            # current = current - 1
            # sum_memory = sum(current/pd)
            while sum_memory == memory:
                current = current - 1
                sum_memory = sum(current//pd)
            return current + 1